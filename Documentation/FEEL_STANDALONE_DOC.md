# Communicate with the RECOVIB FEEL
<!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->

- [Communicate with the RECOVIB FEEL](#communicate-with-the-recovib-feel)
	- [Overview](#overview)
	- [Command Set](#command-set)
		- [Incoming Commands](#incoming-commands)
			- [GetParam](#getparam)
			- [SetParam](#setparam)
			- [StartStreaming](#startstreaming)
			- [ContinueStreaming](#continuestreaming)
			- [StopStreaming](#stopstreaming)
		- [Outcoming Responses](#outcoming-responses)
			- [GetParamReply](#getparamreply)
			- [SetParamReply](#setparamreply)
			- [StartStreamingReply](#startstreamingreply)
			- [ContinueStreamingReply](#continuestreamingreply)
		- [Parameters tables](#parameters-tables)
	- [Quickstart](#quickstart)
		- [Connect to FEEL, start acquisition and start streaming](#connect-to-feel-start-acquisition-and-start-streaming)

<!-- /MarkdownTOC -->

## Overview

Once connected to a USB host (e.g. PC or Android devices[^1]), the RECOVIB FEEL initializes an [HID](https://en.wikipedia.org/wiki/USB_human_interface_device_class) datapipe interface.

The specifications for a RECOVIB FEEL device is :
- **Vendor ID** = 0x2047
- **Product ID**= 0x094A

## Command Set

Through the above described HID interface, the USB host will be able to send the device a set of commands defined below.

The device never sends messages spontaneously. It always respond to a prior received command.

Each message conforms (incoming and outcoming) to the following HID formatting :

- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size**  - The number of valid bytes in the **Data** field (max 62 bytes)
- [2 : 63] : **Data** payload

### Incoming Commands

#### GetParam

- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 2  - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x1B
	+ [1 : 1] : **Parameter ID** (see [Parameters list](#parameters-tables))
	+ [2 : 61] : don't care

Expected Response :

- [GetParamReply](#getparamreply)

#### SetParam

- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 3 + **Parameter Size** - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x1A
	+ [1 : 1] : **Parameter ID** (see [Parameters list](#parameters-tables))
	+ [2 : 2] : **Parameter Type** (see [Parameters list](#parameters-tables))
	+ [3 : (3+**Parameter Size**-1)] : **Parameter Value**
	+ [(3+**Parameter Size**) : 61] : don't care

Expected Response :

- [SetParamReply](#setparamreply)

#### StartStreaming
By sending this command, you reset the acquisition timestamp to 0.

- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 1 - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x1F
	+ [1 : 61] : don't care

Expected Response :

- [StartStreamingReply](#startstreamingreply)

#### ContinueStreaming
- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 1 - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x21
	+ [1 : 61] : don't care

Expected Response :

- [ContinueStreamingReply](#continuestreamingreply)

#### StopStreaming
By sending this command, you reset the acquisition timestamp to 0.

- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 1 - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x20
	+ [1 : 61] : don't care

Expected Response :

- No response to expect from this command

### Outcoming Responses
#### GetParamReply
- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 3 + **Parameter Size** - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x1B
	+ [1 : 1] : **Parameter ID** (see [Parameters list](#parameters-tables))
	+ [2 : 2] : **Parameter Type** (see [Parameters list](#parameters-tables))
	+ [3 : (3+**Parameter Size**-1)] : **Parameter Value**
	+ [(3+**Parameter Size**) : 61] : don't care

#### SetParamReply
- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 2  - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x1A
	+ [1 : 1] : **Parameter ID** (see [Parameters list](#parameters-tables))
	+ [2 : 61] : don't care

#### StartStreamingReply
- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 8 - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 0] : **Command ID** = 0x1F
	+ [1 : 61] : don't care

#### ContinueStreamingReply
- [0 : 0] : **Report ID** = 0x3F (HID-Datapipe)
- [1 : 1] : **Size** = 8 + **Count** * 6 - The number of valid bytes in the **Data** field
- [2 : 63] : **Data** payload
	+ [0 : 1] : **Total Count** ->Number of acquisition vectors[^2] stored on the device side. If this number exceeds 499, then the device's memory overflowed and acquisition data can be corrupted.
	+ [2 : 3] : **Count** -> Number of acquisition vectors[^2] in the current message
	+ [4 : 7] : UINT32 **Tick**. 1 tick = $`\frac{1}{\text{Sampling Rate}}`$ [s]
	+ [8 : 8+(**Count***6)-1] : Acquisition vectors organized as follows :

| $`ACQ_{X,1,LSB}`$ | $`ACQ_{X,1,MSB}`$ | $`ACQ_{Y,1,LSB}`$ | $`ACQ_{Y,1,MSB}`$ | $`ACQ_{Z,1,LSB}`$ | $`ACQ_{Z,1,MSB}`$ | ... | $`ACQ_{X, \textbf{Count},LSB}`$ | $`ACQ_{X, \textbf{Count},MSB}`$ | $`ACQ_{Y, \textbf{Count},LSB}`$ | $`ACQ_{Y, \textbf{Count},MSB}`$ | $`ACQ_{Z, \textbf{Count},LSB}`$ | $`ACQ_{Z, \textbf{Count},MSB}`$ |
|:-----------------:|:-----------------:|:-----------------:|:-----------------:|:-----------------:|:-----------------:|:---:|:--------------------------:|:--------------------------:|:--------------------------:|:--------------------------:|:--------------------------:|:--------------------------:|

### Parameters tables

| **Parameter Description**  | **Parameter ID** | **Parameter Type** | **Parameter Size (bytes)** |
| :-                        | :-              | :-                | :-              |
| Serial Number              | 0x01             | String             | 8                |
| 2g/15g/200g mode X axis gain        | 0x03             | FLOAT32            | 4                |
| 2g/15g/200g mode Y axis gain        | 0x04             | FLOAT32            | 4                |
| 2g/15g/200g mode Z axis gain        | 0x05             | FLOAT32            | 4                |
| 2g/15g/200g mode X axis offset      | 0x06             | FLOAT32            | 4                |
| 2g/15g/200g mode Y axis offset      | 0x07             | FLOAT32            | 4                |
| 2g/15g/200g mode Z axis offset      | 0x08             | FLOAT32            | 4                |
| 6g mode X axis gain        | 0x09             | FLOAT32            | 4                |
| 6g mode Y axis gain        | 0x0A             | FLOAT32            | 4                |
| 6g mode Z axis gain        | 0x0B             | FLOAT32            | 4                |
| 6g mode X axis offset      | 0x0C             | FLOAT32            | 4                |
| 6g mode Y axis offset      | 0x0D             | FLOAT32            | 4                |
| 6g mode Z axis offset      | 0x0E             | FLOAT32            | 4                |
| Sampling Rate              | 0x13             | UINT16             | 2               |
| Sensitivity currently used (0x02 for 2g/15g/200g mode, 0x06 for 6g mode) | 0x17             | INT8               | 1                |


## Quickstart
### Connect to FEEL, start acquisition and start streaming
A very simple streaming application can be implemented following these steps : 

1. Upon device USB enumeration, browse for HID device with VID = 0x2047 and PID = 0x094A. Open a connection with this device
2. Retrieve the device's usefull information using successive call to [GetParam](#getparam):
	- Serial Number
	- Current Sensitivity
	- Sampling Rate
	- Gains for all axes
	- Offsets for all axes
3. Issue the [StartStreaming](#startstreaming) command to reset the acquisition and start streaming
4. Issue the [ContinueStreaming](#continuestreaming) command to receive acquisition data
5. Convert the received INT16 binary data into $`\frac{m}{s^{2}}`$ using the gains and offsets retrieved in step 2.
6. Go to step 4

Refer to the [Examples](../Examples) folder to find these steps in application.

[^1]: [USB OTG](https://fr.wikipedia.org/wiki/USB_On-The-Go)
[^2]: 1 acquisition vector is 6 bytes long (INT16 measure * 3 axes)