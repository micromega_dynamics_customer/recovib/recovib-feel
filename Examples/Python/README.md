# Interfacing RECOVIB FEEL devices through USB HID with Python

![Results](Python_example.PNG)

You should be able to run the provided [script example](feel_python.m) after installing at least

- [Matplotlib](https://matplotlib.org/)
- [Pywinusb](https://github.com/rene-aguirre/pywinusb)

