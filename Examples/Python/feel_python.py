import sys
import time
import os
import numpy as np
import matplotlib.pyplot as plt
import struct
from msvcrt import kbhit
from pywinusb import hid
COLORS = ['b', 'r', 'g']
AXES = ['X', 'Y', 'Z']
MAX_FIFO_SIZE = 499

new_message = False
last_received_buffer = [0xFF]*64

def response_handler(data):
    global new_message
    global last_received_buffer
    last_received_buffer = data
    new_message = True


def get_param(param_id):
    cmd = [0xFF]*64
    cmd[0] = 0x3F
    cmd[1] = 0x02
    cmd[2] = 0x1B
    cmd[3] = param_id
    return cmd

def get_sn(device):
    global new_message
    global last_received_buffer
    new_message = False
    cmd = get_param(0x01)
    report = device.find_output_reports()
    report[0].set_raw_data(cmd)
    report[0].send()
    while(new_message == False):
        pass
    sn = ''
    for i in last_received_buffer[5:(5+8)]:
        sn = sn + chr(i)
    return sn

def get_sensitivity(device):
    global new_message
    global last_received_buffer
    new_message = False
    cmd = get_param(0x17)
    report = device.find_output_reports()
    report[0].set_raw_data(cmd)
    report[0].send()
    while(new_message == False):
        pass
    return last_received_buffer[5]

def get_sampling_rate(device):
    global new_message
    global last_received_buffer
    new_message = False
    cmd = get_param(0x13)
    report = device.find_output_reports()
    report[0].set_raw_data(cmd)
    report[0].send()
    while(new_message == False):
        pass
    b = bytearray(last_received_buffer[5:7])
    return struct.unpack('<h',b)[0]

def get_offsets(device, sensitivity):
    global new_message
    global last_received_buffer
    offsets = np.zeros(3)
    param_id = 0
    if(sensitivity == 6):
        param_id = 0x0C
    else:
        param_id = 0x06
    for i in range(0, 3):
        new_message = False
        cmd = get_param(param_id+i)
        report = device.find_output_reports()
        report[0].set_raw_data(cmd)
        report[0].send()
        while(new_message == False):
            pass
        b = bytearray(last_received_buffer[5:9])
        offsets[i] = struct.unpack('<f',b)[0]

    return offsets

def get_gains(device, sensitivity):
    global new_message
    global last_received_buffer
    gains = np.zeros(3)
    param_id = 0
    if(sensitivity == 6):
        param_id = 0x09
    else:
        param_id = 0x03
    for i in range(0, 3):
        new_message = False
        cmd = get_param(param_id+i)
        report = device.find_output_reports()
        report[0].set_raw_data(cmd)
        report[0].send()
        while(new_message == False):
            pass
        b = bytearray(last_received_buffer[5:9])
        gains[i] = struct.unpack('<f',b)[0]

    return gains

def start_stream(device):
    global new_message
    global last_received_buffer
    cmd = [0xFF]*64
    cmd[0] = 0x3F
    cmd[1] = 0x01
    cmd[2] = 0x1F
    new_message = False
    report = device.find_output_reports()
    report[0].set_raw_data(cmd)
    report[0].send()
    while(new_message == False):
        pass

def continue_stream(device, offsets, gains, ticks, measures):
    global new_message
    global last_received_buffer
    cmd = [0xFF]*64
    cmd[0] = 0x3F
    cmd[1] = 0x01
    cmd[2] = 0x21
    new_message = False
    report = device.find_output_reports()
    report[0].set_raw_data(cmd)
    report[0].send()
    while(new_message == False):
        time.sleep(0.0001)
    total_count_b = bytearray(last_received_buffer[2:4])
    total_count = struct.unpack('<h',total_count_b)[0]
    count_b = bytearray(last_received_buffer[4:6])
    count = struct.unpack('<h',count_b)[0]
    tick_b = bytearray(last_received_buffer[6:10])
    tick = struct.unpack('<I',tick_b)[0]
    if(total_count > MAX_FIFO_SIZE):
        print('Device\'s memory overflowed')
        return False
    for i in range(0,count):
        ticks.append(tick+i) 
        for j in range(0, 3):
            idx = (10 + 6*i + j*2)
            data_b = bytearray(last_received_buffer[idx:idx+2])
            data_i16 = struct.unpack('<h',data_b)[0]
            #5.
            #Convert the received INT16 binary data into ms2 using the gains and offsets retrieved in step 2.
            measures[j].append((data_i16 - offsets[j])*gains[j])
    return True

def stop_stream(device):
    global new_message
    global last_received_buffer
    cmd = [0xFF]*64
    cmd[0] = 0x3F
    cmd[1] = 0x01
    cmd[2] = 0x20
    new_message = False
    report = device.find_output_reports()
    report[0].set_raw_data(cmd)
    report[0].send()

#1.
#Upon device USB enumeration, browse for HID device with VID = 0x2047 and PID = 0x094A. Open a connection with this device   
filter = hid.HidDeviceFilter(vendor_id = 0x2047, product_id = 0x094A)
hid_device = filter.get_devices()
device = hid_device[0]
device.open()
device.set_raw_data_handler(response_handler)

#2.
# Retrieve the device's usefull information using successive call to GetParam:
# Serial Number
sn = get_sn(device)
print(sn)
# Current Sensitivity
sensitivity = get_sensitivity(device)
# Sampling Rate
sampling_frequency = get_sampling_rate(device)
# Offsets for all axes
gains = get_gains(device, sensitivity)
# Gains for all axes
offsets = get_offsets(device, sensitivity)

measures = [[],[],[]]
ticks = []

#3.
#Issue the StartStreaming command to reset the acquisition and start streaming
start_stream(device)
print('Acquisition started')
print('Press any key to plot acquired data and stop acquisition')
#4.
#Issue the ContinueStreaming command to receive acquisition data
while not kbhit() and device.is_plugged() and continue_stream(device, offsets, gains, ticks, measures):
    #just keep the device opened to receive events
    time.sleep(0.0005)

stop_stream(device)

t = []
for tick in ticks:
    t.append(tick/sampling_frequency)
for j in range(0, 3):
    plt.subplot(3, 1, j+1)
    plt.plot(t, measures[j], COLORS[j])
    plt.ylabel(AXES[j])

plt.xlabel('Time [s]')
plt.show()