## RECOVIB FEEL

Repository containing any scripts/documentation/tools related to the RECOVIB FEEL.

You can find the datasheet on the following page: [https://micromega-dynamics.com/products/recovib/usb-accelerometer/](https://micromega-dynamics.com/products/recovib/usb-accelerometer/)